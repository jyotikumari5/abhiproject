package com.bby.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bby.authorization.BigQueryAuthorization;
import com.bby.bean.Employee;
import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.model.TableRow;

@RestController
public class BigQueryController {

	String projectId="abhiproject-200910";

	@Autowired
	BigQueryAuthorization auth;

	@RequestMapping(path="/showResults",method=RequestMethod.GET)
	public List<Employee> tableData() throws IOException{
		List<Employee> list=new ArrayList<Employee>();
		Bigquery bigquery=auth.createAuthorizedClient();
		List<TableRow> rows =
				auth.executeQuery(
						"SELECT id,name FROM OwnDataset.Employy",
						bigquery,
						projectId);

		for (TableRow row : rows) {
			Employee e=new Employee();
			e.setId(Integer.parseInt( (String) row.getF().get(0).getV()));
			e.setName((String) row.getF().get(1).getV());
			list.add(e);
		}
		
		System.out.println(list);
		return list;

	}
}
